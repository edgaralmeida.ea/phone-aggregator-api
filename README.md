Phone Aggregator API
---

### Introduction

This is a spring boot API that exposes only one post endpoint, consumes data from [https://challenge-business-sector-api.meza.talkdeskstg.com/sector/] api and aggregates the result by sector.

### Git Clone
- https://gitlab.com/edgaralmeida.ea/phone-aggregator-api.git

### Start the App

./mvnw spring-boot:run

Using docker:

- docker-compose up web 

server port 8080 defined on docker-compose.yml

### Swagger Ui
http://localhost:8080/swagger-ui.html

### Actuators
Info -> http://localhost:8080/management/info
Health -> http://localhost:8080/management/health 
Metrics -> http://localhost:8080/management/metrics
Logs -> http://localhost:8080/management/loggers

### Endpoint definition
@Path - http://localhost:8080/api/aggregate/

Required Parameters:

@RequestBody String[] with phone numbers -> required

Example: ["+1983248", "001382355", "+147 8192", "+4439877"]


Http Return status:

- 200 - OK 
Schema:
{
  "type": "object",
  "additionalProperties": {
    "type": "object",
    "additionalProperties": {
      "type": "number"
    }
  }
}

Example:
{
	"1": {
		"Clothing": 1,
		"Technology": 2
	},
	"44": {
		"Banking": 1
	}
}

- 400 - Bad Request

- 500 - Internal Server Error

### Service
@Service - > PhoneAggregatorService

Exposes aggregate method.

### TalkDesk API
@SectorResquest
https://challenge-business-sector-api.meza.talkdeskstg.com/sector/{PhoneNumber}

@SectorResponse
{
  "number": "String",
  "sector": "String"
}





