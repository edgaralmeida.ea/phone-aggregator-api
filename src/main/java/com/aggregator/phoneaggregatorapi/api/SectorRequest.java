package com.aggregator.phoneaggregatorapi.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SectorRequest {

    @JsonProperty("phoneNumber")
    private String phoneNumber;

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
