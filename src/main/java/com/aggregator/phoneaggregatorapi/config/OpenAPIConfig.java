package com.aggregator.phoneaggregatorapi.config;

import com.aggregator.phoneaggregatorapi.web.rest.dto.PhoneAggregatorDTO;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.media.ObjectSchema;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.media.StringSchema;

@Configuration
@OpenAPIDefinition
public class OpenAPIConfig {

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI().components(new Components().addSchemas("PhoneAggregatorDTO",
                new Schema<PhoneAggregatorDTO>().addProperties("type", new StringSchema().example("object"))
                        .addProperties("additionalProperties", new ObjectSchema()
                                .addProperties("type", new StringSchema().example("object"))
                                .addProperties("additionalProperties",
                                        new ObjectSchema().addProperties("type", new StringSchema().example("number"))))

        ));
    }
}
