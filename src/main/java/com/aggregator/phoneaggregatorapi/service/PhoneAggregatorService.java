package com.aggregator.phoneaggregatorapi.service;

import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;

import com.aggregator.phoneaggregatorapi.api.SectorResponse;
import com.aggregator.phoneaggregatorapi.utils.Utils;
import com.aggregator.phoneaggregatorapi.web.rest.dto.PhoneAggregatorDTO;

import org.apache.commons.lang3.ArrayUtils;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import io.micrometer.core.instrument.util.StringUtils;

@EnableCaching
@Service
public class PhoneAggregatorService {

    @Autowired
    private TalkDeskAPIService talkDeskAPIService;

    public Optional<PhoneAggregatorDTO> aggregate(String[] phoneNumberList) {

        // List of prefixes present on txt file
        List<String> prefixesList = Utils.prefixes;

        if (ArrayUtils.isEmpty(phoneNumberList) || prefixesList.isEmpty()) {
            return null;
        }

        // Final Response
        PhoneAggregatorDTO phoneAggregatorResponseDTO = new PhoneAggregatorDTO();

        for (String phoneNumber : phoneNumberList) {

            if (!StringUtils.isBlank(phoneNumber)) {

                // valid prefix needs to be present on the list
                Optional<String> prefix = prefixesList.stream().filter(p -> p.equals(phoneNumber
                        .replaceAll("^[+]|^0+", "").substring(0, phoneNumber.length() >= p.length() ? p.length() : 1)))
                        .findAny();

                if (prefix.isPresent()) {

                    // call talkDesk API
                    ResponseEntity<SectorResponse> response = talkDeskAPIService.callTalkDeskApi(phoneNumber);

                    if (response != null) {
                        // Store sector string
                        String sector = response.getBody().getSector();

                        // fill tuple to store on phoneAggregatorResponseDTO
                        Pair<String, String> pair = Pair.with(prefix.get(), sector);

                        // add valid response to phoneAggregatorResponseDTO
                        aggregator(pair, phoneAggregatorResponseDTO);
                    }

                }
            }
        }

        return Optional.of(phoneAggregatorResponseDTO);

    }

    /**
     * @Param Tuple Pair of String, String
     * @Param PhoneAggregatorDTO
     * 
     *        Add response from TalkDeskApi to DTO
     */
    private void aggregator(Pair<String, String> pair, PhoneAggregatorDTO phoneAggregatorResponseDTO) {

        Integer sectorCounter = 1;
    
        // add to an existing key
        if (phoneAggregatorResponseDTO.containsKey(pair.getValue0())) {

            Optional<Entry<String, Integer>> map = phoneAggregatorResponseDTO.get(pair.getValue0()).entrySet().stream()
                    .filter(a -> a.getKey().equals(pair.getValue1())).findAny();

            //if the sector already exists -> increment counter        
            if (map.isPresent()) {
                sectorCounter = map.get().getValue();
                sectorCounter++;
            }

            phoneAggregatorResponseDTO.get(pair.getValue0()).put(pair.getValue1(), sectorCounter);
        } else {

            // create a new key
            SortedMap<String, Integer> apiResult = new TreeMap<String, Integer>();

            apiResult.put(pair.getValue1(), sectorCounter);

            phoneAggregatorResponseDTO.put(pair.getValue0(), apiResult);

        }
    }
}
