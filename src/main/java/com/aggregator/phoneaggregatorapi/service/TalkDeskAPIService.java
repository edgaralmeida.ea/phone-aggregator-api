package com.aggregator.phoneaggregatorapi.service;

import com.aggregator.phoneaggregatorapi.api.SectorResponse;
import com.aggregator.phoneaggregatorapi.contants.Constants;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.HttpClientErrorException.BadRequest;

@EnableCaching
@Service
public class TalkDeskAPIService {

    @Autowired
    private RestTemplate restTemplate;

    private static final Logger log = LoggerFactory.getLogger(PhoneAggregatorService.class);

    /**
     * Get phone number sector.
     *
     * @param phoneNumber
     * @return ResponseEntity<SectorResponse> with details
     * 
     *         This method implements a cache mechanism and store it on "sectors".
     */
    @Cacheable(value= "sectors", key="#phoneNumber", condition = "#phoneNumber != null")
    public ResponseEntity<SectorResponse> callTalkDeskApi(String phoneNumber) {

        log.debug("callTalkDeskApi(phoneNumber: {})", phoneNumber);
        if (StringUtils.isEmpty(phoneNumber)) {
            return null;
        }

        ResponseEntity<SectorResponse> response = null;
        try {

            response = restTemplate.getForEntity(Constants.TALK_DESK_API_URL.replace("{phoneNumber}", phoneNumber),
                    SectorResponse.class);

        } catch (BadRequest e) {
            log.info(e.getMessage());
        }

        return response;
    }

}
