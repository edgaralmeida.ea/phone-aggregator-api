package com.aggregator.phoneaggregatorapi.web.rest.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.aggregator.phoneaggregatorapi.service.PhoneAggregatorService;
import com.aggregator.phoneaggregatorapi.utils.ResponseUtils;
import com.aggregator.phoneaggregatorapi.web.rest.dto.PhoneAggregatorDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zalando.problem.Problem;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RequestMapping(value = "/api")
@RestController
public class PhoneAggregatorController {

    @Autowired
    private PhoneAggregatorService phoneAggregatorService;

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(ref = "#/components/schemas/PhoneAggregatorDTO"))),
            @ApiResponse(responseCode = "400", content = @Content(mediaType = MediaType.APPLICATION_PROBLEM_JSON_VALUE, schema = @Schema(implementation = Problem.class))),
            @ApiResponse(responseCode = "500", content = @Content(mediaType = MediaType.APPLICATION_PROBLEM_JSON_VALUE, schema = @Schema(implementation = Problem.class))) })
    @PostMapping(value = "/aggregate", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<PhoneAggregatorDTO> aggregate(HttpServletRequest request,
            @Valid @NotNull @RequestBody String[] body) {

        Optional<PhoneAggregatorDTO> phoneAggregatorDTO = phoneAggregatorService.aggregate(body);

        return ResponseUtils.okOrBadRequest(MediaType.APPLICATION_JSON, phoneAggregatorDTO);
    }

}
