package com.aggregator.phoneaggregatorapi.contants;

public class Constants {   

    // url to call talkdesk api
    public static final String TALK_DESK_API_URL = "https://challenge-business-sector-api.meza.talkdeskstg.com/sector/{phoneNumber}";

    // validation prefixes file path
    public static final String PATH_TO_PREFIXES_FILE = "static/prefixes.txt";
    
}
