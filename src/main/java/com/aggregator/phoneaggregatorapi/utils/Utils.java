package com.aggregator.phoneaggregatorapi.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import com.aggregator.phoneaggregatorapi.contants.Constants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.ResourceLoader;

public class Utils {

    @Autowired
    ResourceLoader resourceLoader;

    public static List<String> prefixes = null;

    // read prefixes.txt and creates a list of string
    public static void readFileToList() {

        InputStream resource = null;
        try {
            resource = new ClassPathResource(Constants.PATH_TO_PREFIXES_FILE).getInputStream();
        } catch (IOException e2) {
            e2.printStackTrace();
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(resource));
        prefixes = reader.lines().collect(Collectors.toList());

    }

}
