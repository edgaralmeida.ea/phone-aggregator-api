package com.aggregator.phoneaggregatorapi.utils;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

public class ResponseUtils {

    public static <T> ResponseEntity<T> okOrBadRequest(MediaType contentType, Optional<T> maybeResponse) {
        return maybeResponse == null ? ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(contentType).build()
                : maybeResponse.map(response -> ResponseEntity.ok().contentType(contentType).body(response))
                        .orElse(ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(contentType).build());
    }

}
