package com.aggregator.phoneaggregatorapi;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.aggregator.phoneaggregatorapi.utils.Utils;
import com.aggregator.phoneaggregatorapi.web.rest.controller.PhoneAggregatorController;
import com.aggregator.phoneaggregatorapi.web.rest.dto.PhoneAggregatorDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;

@SpringBootTest
class PhoneAggregatorApiApplicationIntTests {

	private MockMvc restMockMvc;

	@Autowired
	private PhoneAggregatorController phoneAggregatorController;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private Validator validator;

	@Autowired
	private ObjectMapper objectMapper;


	@BeforeAll
	public static void readPrefixesFile() {

         Utils.readFileToList();

	}

	@BeforeEach
	public void setup() {

		this.restMockMvc = MockMvcBuilders.standaloneSetup(phoneAggregatorController)
				.setMessageConverters(jacksonMessageConverter).setValidator(validator).build();

	}

	@Test
	void contextLoads() {
	}

	@Test
	public void callAggregateWithoutContentType() throws Exception {

		String[] request = null;

		restMockMvc.perform(post("/api/aggregate").content(objectMapper.writeValueAsString(request)))
				.andExpect(status().isUnsupportedMediaType());

	}

	@Test
	public void callAggregateWithNullRequestArray() throws Exception {

		String[] request = null;

		restMockMvc
				.perform(post("/api/aggregate").contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(request)))
				.andExpect(status().isBadRequest());

	}

	
	@Test
	public void callAggregateWithEmptyRequestArray() throws Exception {

		String[] request = {""};

		MvcResult result = restMockMvc
				.perform(post("/api/aggregate").contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(request)))
				.andExpect(status().isOk()).andReturn();

		PhoneAggregatorDTO phoneAggregatorDTO = objectMapper.readValue(result.getResponse().getContentAsString(), PhoneAggregatorDTO.class);	

		assertTrue(phoneAggregatorDTO.isEmpty());

	}

	@Test
	public void callAggregateWithInvalidStringRequestArray() throws Exception {

		String[] request = {"phoneNumber"};

		MvcResult result = restMockMvc
				.perform(post("/api/aggregate").contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(request)))
				.andExpect(status().isOk()).andReturn();

		PhoneAggregatorDTO phoneAggregatorDTO = objectMapper.readValue(result.getResponse().getContentAsString(), PhoneAggregatorDTO.class);	

		assertTrue(phoneAggregatorDTO.isEmpty());

	}

	@Test
	public void callAggregateWithValidStringArray() throws Exception {

		String[] request = {"+1983248"};
		String expectedPrefix = "1";
	
		MvcResult result = restMockMvc
				.perform(post("/api/aggregate").contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(request)))
				.andExpect(status().isOk()).andReturn();

		PhoneAggregatorDTO phoneAggregatorDTO = objectMapper.readValue(result.getResponse().getContentAsString(), PhoneAggregatorDTO.class);	

		assertTrue(phoneAggregatorDTO.size()==1);
		assertNotNull(phoneAggregatorDTO.get(expectedPrefix));
		assertNotNull(phoneAggregatorDTO.get(expectedPrefix).firstKey());

	}


}
