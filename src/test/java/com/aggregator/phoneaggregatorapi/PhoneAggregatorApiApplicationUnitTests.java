package com.aggregator.phoneaggregatorapi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import com.aggregator.phoneaggregatorapi.api.SectorResponse;
import com.aggregator.phoneaggregatorapi.service.PhoneAggregatorService;
import com.aggregator.phoneaggregatorapi.service.TalkDeskAPIService;
import com.aggregator.phoneaggregatorapi.utils.Utils;
import com.aggregator.phoneaggregatorapi.web.rest.dto.PhoneAggregatorDTO;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest
class PhoneAggregatorApiApplicationUnitTests {

    @Autowired
    private PhoneAggregatorService phoneAggregatorService;

    @Autowired
    private TalkDeskAPIService talkDeskAPIService;

    @BeforeAll
    public static void readPrefixesFile() {

        Utils.readFileToList();
    }

    @Test
    void contextLoads() {
    }

    @Test
    void callServicAggregateWithNullableParameters() {

        Optional<PhoneAggregatorDTO> phoneAggregatorDTO = phoneAggregatorService.aggregate(null);

        assertNull(phoneAggregatorDTO);

    }

    @Test
    void callServicAggregateWithEmptyParameters() {

        String[] phoneNumberList = {};

        Optional<PhoneAggregatorDTO> phoneAggregatorDTO = phoneAggregatorService.aggregate(phoneNumberList);

        assertNull(phoneAggregatorDTO);

    }

    @Test
    void callServicAggregateWithInvalidParameters() {

        String[] phoneNumberList = { "string" };

        Optional<PhoneAggregatorDTO> phoneAggregatorDTO = phoneAggregatorService.aggregate(phoneNumberList);

        assertTrue(phoneAggregatorDTO.isPresent());
        assertNotNull(phoneAggregatorDTO.get());

    }

    @Test
    void callServicAggregateWithValidParameters() {

        String[] phoneNumberList = { "+1983248" };

        Optional<PhoneAggregatorDTO> phoneAggregatorDTO = phoneAggregatorService.aggregate(phoneNumberList);

        assertTrue(phoneAggregatorDTO.isPresent());
        assertNotNull(phoneAggregatorDTO.get());
        assertTrue(phoneAggregatorDTO.get().size() > 0);

    }

    @Test
    void callServiceTalkDeskApiWithNullableParameter() {

        ResponseEntity<SectorResponse> response = talkDeskAPIService.callTalkDeskApi(null);
        assertNull(response);

    }

    @Test
    void callServiceTalkDeskApiWithEmptyParameter() {

        String phoneNumber = "";

        ResponseEntity<SectorResponse> response = talkDeskAPIService.callTalkDeskApi(phoneNumber);
        assertNull(response);

    }

    @Test
    void callServiceTalkDeskApiWithInvalidParameter() {

        String phoneNumber = "+123AAAA";

        ResponseEntity<SectorResponse> response = talkDeskAPIService.callTalkDeskApi(phoneNumber);
        assertNull(response);

    }

    @Test
    void callServiceTalkDeskApiWithValidParameter() {

        String phoneNumber = "+1382355";

        ResponseEntity<SectorResponse> response = talkDeskAPIService.callTalkDeskApi(phoneNumber);
        
        assertNotNull(response.getStatusCode());
        assertEquals(response.getStatusCode(),HttpStatus.OK);
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getSector());
        assertNotNull(response.getBody().getNumber());


    }





}
